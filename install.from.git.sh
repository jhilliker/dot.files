#!/bin/sh -e

# curl -L https://bit.ly/2lvgjcW | sh -s

readonly scriptREPO='https://gitlab.com/jhilliker/dot.files.git'
readonly script='./install.sh'

readonly runFromGitURL="${scriptREPO%.git}/raw/master/run.from.git.sh"

printf -- 'Fetching %s\n' "$runFromGitURL" >&2
curl -L "$runFromGitURL" \
|	sh -s "$scriptREPO" "$script"
