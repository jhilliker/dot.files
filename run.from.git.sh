#!/bin/sh

[ "$#" -ge 2 ] || {
	cat >&2 <<- EOF
		Error: Insufficient arguments. Was $#, need 2+.
		Usage: run.from.git.sh repoURL installScript...
	EOF
	exit 1
}

type log >/dev/null 2>&1 || log() {
	printf '***** %s\n' "$*"
} >&2

type fatal >/dev/null 2>&1 || fatal() {
	log FATAL "$*"
	exit 1
}

readonly repoURL="$1"
shift 1

cd "$(mktemp -d)" \
&& trap "rm -rf '$PWD'" EXIT \
&& log "Working in temporary directory: '$PWD'" \
|| fatal "Couldn't CD to temporary directory"

log "Cloning repository: '$repoURL'"
git clone --depth 1 "$repoURL" || fatal "Error cloning: '$repoURL'"
log 'Done clone.'
cd * || fatal "Couldn't cd to repo dir"

readonly toSrc='./profile.d/log.sh profile.d/colours.sh'
for src in $toSrc ; do
	[ -f "$src" ] && . "$src" || fatal "Couldnt src: '$src'"
done

runScript() {
	(
		readonly installScriptSrc="${1##*/}"
		readonly installScriptDir="${1%/*}"

		cd "$installScriptDir" || exit 1
		log INFO "PWD: '$PWD'"
		log INFO "Running install script: '$installScriptSrc'"

		chmod u+x ./"$installScriptSrc" || exit 1
		./"$installScriptSrc"
	)
}

log INFO "Running install script(s)"
unset failures
unset successes
for script in "$@" ; do
	if runScript "$script" ; then
		successes="$successes
			$script"
	else
		failures="$failures
			$script"
	fi
done

[ -n "$successes" ] && log INFO "Succeeded:$successes"
[ -n "$failures" ] && log ERR "Failures:$failures"

[ -z "$failures" ]
