#!/bin/sh

isdefined () {
	type "$1" > /dev/null 2>&1
}

isdefined log || log() {
	printf '*** %s\n' "$*"
} >&2
isdefined fatal || fatal() {
	log FATAL "$@"
	exit 1
}

[ "$USERDNSDOMAIN" = 'INT.AD.LANGARA.CA' ] || {
	fatal "Not at langara. Exiting."
}
if isdefined node ; then
	log INFO "node already on path. Exiting."
	which node
	node -v
	exit 0
fi

__nodepath_PScommand() {
	local toAdd='$toAdd'
	local oldPath='$oldPath'
	local newPath='$newPath'
	cat <<- EOF
	&{
		$toAdd='$1'
		if (!(Test-Path "$toAdd")) {
			echo "does not exist: '$toAdd'";
			exit 1;
		} else {
			echo "Permanently adding to user path: '$toAdd'";
			$oldPath = [Environment]::GetEnvironmentVariable('path', 'user');
			$newPath = "$oldPath\$(If ($oldPath.endswith(';')) {''} Else {';'})$toAdd;";
			[Environment]::SetEnvironmentVariable('path', "$newPath", 'user');
			$newPath = [Environment]::GetEnvironmentVariable('path', 'user');
			echo "Path is now: $newPath";
		}
	}
	EOF
}

__nodepath_addToUserPathViaPS() {
	[ -d "$1" ] || return 1
	log INFO "Adding to user path via powershell: '$1'"
	local powershellCmd="$(__nodepath_PScommand "$(cygpath -w "$1")" )"
	if ! powershell -Command "$powershellCmd" ; then
		fatal "***Failed*** to add node to path"
	fi

	if [ "$(basename $0)" = 'langara.node.path.sh' ] ; then
		#invoked, not sourced
		cat <<- EOF
			Global user path changed.
			***************************
			** You must restart bash **
			***************************
		EOF
		read -n 1 -t 2 DUMMY
		exit 0
	fi
	return 0
}

__nodepath_addToLocalPath() {
	[ -d "$1" ] || return 1
	log INFO "Adding to local path: '$1'"
	PATH="$PATH:$1"

	local cmd=""
	for cmd in node npm npx ; do
		log INFO "Testing $cmd"
		which "$cmd" && "$cmd" -v || return 1
	done
}

__nodepath_addNodeToPath() {
	local nodepath=""
	# check the O drive at Langara
	[ -z "$nodepath" ] && [ -d "/o" ] && {
		log INFO "Searching the '/o' drive for 'node.exe'."
		# finds the latest version
		nodepath="$(
			set /o/CPSC2350*/'Course Materials/node-v'*'-win-x64/node.exe'
			for n in "$@"; do
				if [ -r "$n" ] ; then
					echo "$n" \
						| grep -oE 'v[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+' \
						| tr -dc '[:digit:].'
					printf ' %s\n' "$n"
				fi
			done | sort -Vr | head -1 | grep -o '/.*')"
	}
	nodepath="${nodepath%/node.exe}"
	if [ -d "$nodepath" ] ; then
		log INFO "'node' found at: '$nodepath'"

		__nodepath_addToUserPathViaPS "$nodepath" \
		&& __nodepath_addToLocalPath "$nodepath"
		return $?
	else
		fatal "'node' not found"
	fi
}
__nodepath_addNodeToPath \
|| fatal
