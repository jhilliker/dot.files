#! /bin/sh

readonly configSrc='atom.config.cson'
readonly atomConfig="$HOME/.atom"
readonly configDst="$atomConfig/${configSrc#atom.}"
readonly packages='
minimap
highlight-selected
minimap-highlight-selected
pigments
minimap-pigments
minimap-bookmarks
minimap-cursorline
minimap-find-and-replace
minimap-git-diff
minimap-selection
better-git-blame
'

type log > /dev/null 2>&1 || log() {
	printf '***** %s\n' "$*"
} >&2

fatal() {
	[ "$#" -ne '0' ] && log FATAL "$*"
	[ -t '0' ] || [ -p '/dev/stdin' ] && {
		log 'press any key...'
		read -n 1 -t 30 REPLY
	}
	exit 1
}

parseArgs() {
	local options="$(getopt -o 'P' -l 'no-packages' -n "$0" -- "$@")"
	[ $? != 0 ] && fatal "Invalid arguments: $@"
	eval set -- "$options"
	while : ; do
		case "$1" in
			-P|--no-packages)
				opt_noPackages='t'; shift ;;
			--) shift; break ;;
			*)
				fatal "Invalid arguments: '$1'";;
		esac
	done
	[ $? != 0 ] && fatal "Invalid arguments: $@"
}
parseArgs "$@"

[ -f "$configSrc" ] || fatal "Missing config file: '$configSrc'"

# #########################################################

findAPM() {
	unset apm
	apm="$(which apm)"
	[ -x "$apm" ] && return 0

	case "$(uname -s)" in
		MINGW*)  ;;
		CYGWIN*) ;;
		*) return 1 ;;
	esac

	# windows
	local readonly LOCALAPPDATA="$(cygpath -u "$LOCALAPPDATA")"
	local atomPath=''
	[ -z "$atomPath" ] && [ -x "$LOCALAPPDATA/atom/atom.exe" ] && \
		atomPath="$LOCALAPPDATA/atom/atom.exe"
	[ -z "$atomPath" ] && {
		set "$(cygpath -u "$PROGRAMFILES")"/[aA]tom*/atom.exe
		[ "$#" -ge "2" ] && fatal "Too much atom: $@"
		[ -x "$1" ] && atomPath="$1"
	}
	[ -z "$atomPath" ] && {
		set "$LOCALAPPDATA/Microsoft/AppV/Client/Integration/"*'/Root/VFS/ProgramFilesX64'/[aA]tom*/atom.exe
		[ -x "$1" ] && {
			## appV VFS
			log "$1" >&2
			log "Found appV exe. Launching virtual process." >&2
			AppVName='$AppVName'
			powerShCmd="
			$AppVName = Get-AppvClientPackage Atom*
			echo $AppVName
			Start-AppvVirtualProcess -Wait \`
				-LoadUserProfile \`
				-AppvClientObject $AppVName \`
				-FilePath '$(cygpath -w "$BASH")' \`
				-ArgumentList @( '$(cygpath -ua "$0")' )
			exit \$?
			"
			log "$powerShCmd"
			if powershell -Command "$powerShCmd" ; then
				log "success"
				exit 0
			else
				fatal "powershell failed"
			fi
		}
	}
	local readonly atomPath="${atomPath%/atom.exe}"

	local readonly apms='
		resources/cli/apm
		bin/apm
		resources/app/apm/bin/apm
	'
	for apm in $apms ; do
		apm="$atomPath/$apm"
		[ -f "$apm" ] && return 0
		apm="$apm.sh"
		[ -f "$apm" ] && return 0
	done
	apm="$(find "$atomPath" -maxdepth 6 -type f \
		\( -name 'apm' -o -name 'apm.sh' \) -print -quit)"
	[ -f "$apm" ] && return 0
	apm=''
	return 1
}
[ -z "$opt_noPackages" ] && findAPM

# #########################################################

# backup old config
unset configBak
[ -s "$configDst" ] && {
	configBak="$(mktemp -t "tmp.XXXXXX.$configSrc")"
	cp -pv "$configDst" "$configBak"
}

if [ -d "$atomConfig" ] ; then
	( cd "$atomConfig" && rm -rf * .[^.]* ..?* ) # keeps symlink
else
	rm -rf "$atomConfig" && mkdir -p "$atomConfig"
fi || fatal "Unable to remove config dir: '$atomConfig'"

# restore backup
[ -s "$configBak" ] && {
	mv -v "$configBak" "$configDst" || fatal "Couldn't restore backup"
}
# copy new config
cp -vbp "$configSrc" "$configDst" || fatal "Couldn't copy new config"


# install packages
if [ -z "$opt_noPackages" ] && [ -n "$packages" ] ; then
	if [ -f "$apm" ] ; then
		"$apm" install $packages || fatal "Failed installing packages with '$apm'"
	else
		fatal "Couldn't find 'apm' to install packages."
	fi
else
	echo "Not installing packages."
fi

echo "done" >&2
exit 0
