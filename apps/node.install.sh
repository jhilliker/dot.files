#!/bin/sh -xe

type log > /dev/null 2>&1 || log() {
	printf '***** %s\n' "$*"
} >&2

fatal() {
	[ $# -gt 0 ] && log FATAL "$*"
	exit 1
}

isdefined() {
	type $1 >/dev/null 2>&1
}

[ "$USER" = "root" ] || fatal 'must run as root'
{ uname -a | grep -i 'linux' ; } || fatal 'linux only'

cd /tmp

if isdefined apt-get ; then
	# https://github.com/nodesource/distributions
	curl -sL https://deb.nodesource.com/setup_12.x | bash -
	apt-get update
	apt-get install nodejs
elif isdefined pacman ; then
	pacman -S nodejs-lts-erbium npm
else
	fatal 'unknown distro'
fi

log 'instal complete'
exit 0
