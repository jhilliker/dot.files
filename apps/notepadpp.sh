#!/bin/sh -e

type log > /dev/null 2>&1 || log() {
	printf '***** %s\n' "$*"
} >&2

fatal() {
	log FATAL "$*"
	exit 1
} >&2

case "$(uname -s)" in
	MINGW*)  ;;
	CYGWIN*) ;;
	*)       fatal "This script is for Windows only." ;;
esac

themes='
	https://raw.githubusercontent.com/dracula/notepad-plus-plus/master/Dracula.xml
	https://raw.githubusercontent.com/HiSandy/npp-material-theme/master/Material%20Theme.xml
'

readonly APPDATA="$(cygpath -u "$APPDATA")"
readonly appDir="$APPDATA/Notepad++"
[ -d "$appDir" ] || fatal "'$appDir' missing."
readonly themeDir="$appDir/themes"
readonly configFile="$appDir/config.xml"

setValues() {
	local fname="$1"
	local lineId="$2"
	shift 2
	local rules=''
	for r in "$@" ; do
		local key=${r%%=*}
		local val=${r##*=}
		local rule="s|$key=\"[^\"]*\"|$key=$val|"
		rules="$rules"$'\n'"$rule"
	done

	sed -E "/$lineId/ {
		$rules
	}" -i "$fname"
}

mkdir -vp "$themeDir"
for themeURL in $themes ; do
	themeFile="${themeURL##*/}"
	themeFile="$(echo "$themeFile" | sed 's/%20/ /g')"
	themeFile="$themeDir/$themeFile"
	themeFileWin="$(cygpath -w "$themeFile" | sed 's|\\|\\\\|g')"
	echo "Installing '$themeFile'"
	curl -L "$themeURL" -o "$themeFile" && {
		setValues "$configFile" '<GUIConfig name="stylerTheme"' \
			"path=\"$themeFileWin\""
		[ "${themeFile##*/}" = "Material Theme.xml" ] && {
			setValues "$themeFile" '<WidgetStyle name="White space symbol"' \
				'fgColor="554433"'
			setValues "$themeFile" '<WidgetStyle name="Edge colour"' \
				'fgColor="556677"'
		}
	}
done

echo "Modifying '$configFile'"
cp "$configFile" "$configFile~"
setValues "$configFile" '<GUIConfig name="ScintillaPrimaryView"' \
	'lineNumberMargin="show"'\
	'indentGuideLine="show"'\
	'lineWrapMethod="indent"'\
	'currentLineHilitingShow="show"'\
	'scrollBeyondLastLine="yes"'\
	'wrapSymbolShow="show"'\
	'Wrap="yes"'\
	'borderEdge="yes"'\
	'edge="line"'\
	'edgeNbColumn="80"'\
	'zoom="8"'\
	'whiteSpaceShow="show"'\
	'eolShow="hide"'\
	'borderWidth="1"'
setValues "$configFile" '<GUIConfig name="NewDocDefaultSettings"' \
	'format="2"'\
	'encoding="4"'
setValues "$configFile" '<GUIConfig name="TabSetting"' \
	'replaceBySpace="no"'\
	'size="2"'
