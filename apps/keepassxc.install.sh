#!/bin/sh -xe

type log > /dev/null 2>&1 || log() {
	printf '***** %s\n' "$*"
} >&2

fatal() {
	[ $# -gt 0 ] && log FATAL "$*"
	exit 1
}

isdefined() {
	type $1 >/dev/null 2>&1
}

[ "$USER" = "root" ] || fatal 'must run as root'
{ uname -a | grep -i 'linux' ; } || fatal 'linux only'

cd /tmp

log 'Installing keepassxc'

if isdefined apt-get ; then
	add-apt-repository ppa:phoerious/keepassxc
	apt-get update
	apt-get install keepassxc
elif isdefined pacman ; then
	pacman -S keepassxc
else
	fatal 'unknown distro'
fi

log 'Install complete'
exit 0
