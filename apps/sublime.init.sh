#!/bin/sh -e

type log > /dev/null 2>&1 || log() {
	printf '***** %s\n' "$*"
} >&2

fatal() {
	log FATAL "$*"
	exit 1
}

log INFO 'Determining OS & config path...'
case "$(uname -s)" in
	MINGW*|CYGWIN*)
		fatal 'Linux only'
		# configPath="$(cygpath "$APPDATA")/Sublime Text 3"
		;;
	Linux)
		log DEBUG 'Linux'
		configPath="$HOME/.config/sublime-text"
		;;
	*)
		fatal "Unknown OS: '$(uname -s)'"
		;;
esac
readonly configPath
log DEBUG "'$configPath'"

log INFO 'Creating user config dir...'
mkdir -p \
	"$configPath/Packages/User/" \
	|| fatal

log INFO 'Copying config file...'
cp -pTv \
	'sublime.config.json' \
	"$configPath/Packages/User/Preferences.sublime-settings" \
	|| fatal

log WARN 'Install the following packages:
	Package Control
	TypeScript
	AFileIcon
	TrailingSpaces
	Single Trailing Newline
	SideBarEnhancements
	SublimeText-Nodejs
'

exit 0
