#!/bin/sh -xe

type log > /dev/null 2>&1 || log() {
	printf '***** %s\n' "$*"
} >&2

fatal() {
	[ $# -gt 0 ] && log FATAL "$*"
	exit 1
}

isdefined() {
	type $1 >/dev/null 2>&1
}

[ "$USER" = "root" ] || fatal 'must run as root'
{ uname -a | grep -i 'linux' ; } || fatal 'linux only'

log 'Installing Kodi'

if isdefined apt-get ; then
	apt-get update
	apt-get install software-properties-common
	add-apt-repository ppa:team-xbmc/ppa
	apt-get update
	apt-get install kodi
else
	fatal 'unknown distro'
fi

log 'Install complete'
exit 0
