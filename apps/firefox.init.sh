#! /bin/sh

readonly configSrc='firefox.user.js'
addons='
enhanced-steam-an-itad-fork
clear-browsing-data
clearurls
cookie-autodelete
cookie-quick-manager
darkreader
enhanced-h264ify
facebook-container
gnome-shell-integration
keepassxc-browser
react-devtools
reviewmeta-com-review-helper
search_by_image
sponsorblock
tree-style-tab
ublock-origin
video-downloadhelper
violentmonkey
'

# #########################################################

type log > /dev/null 2>&1 || log() {
	printf '***** %s\n' "$*"
} >&2

fatal() {
	log FATAL "$*"
	exit 1
}

findFirefoxConfig() {
	firefoxConfig="$HOME/.mozilla/firefox"
	[ -d "$firefoxConfig" ] && return 0

	case "$(uname -s)" in
		MINGW*)  ;;
		CYGWIN*) ;;
		*)       return 1
	esac

	# windows
	local readonly APPDATA="$(cygpath -u "$APPDATA")"
	firefoxConfig="$APPDATA/Mozilla/Firefox"
	[ -d "$firefoxConfig" ]
}

# #########################################################

[ -f "$configSrc" ] || fatal "Missing config file: '$configSrc'"
findFirefoxConfig || fatal "Couldnt find Firefox config dir"

rm -rf "$HOME/.cache/mozilla/"

rm -rf "$firefoxConfig/Crash Reports"

find "$firefoxConfig" -type f -name 'prefs.js' | (
	while IFS= read -r REPLY ; do

		readonly profileDir="${REPLY%%/prefs.js}"
		log "Found: '$profileDir'"

		rm -rf "$profileDir/crashes" "$profileDir/datareporting" \
			"$profileDir/minidumps" "$profileDir/sessionstore-backups"

		readonly configDst="$profileDir/${configSrc#firefox.}"
		if cp -vbp "$configSrc" "$configDst" ; then
			log "Copied to '$configDst'"
		else
			fatal "ERR copying to '$configDst'"
		fi
	done
) || fatal "Config not copied"

addons="$(
	printf '%s\n' "$addons" | tr '[:space:]' '\n' | sed \
	-e '/\S/! d' \
	-e 's,^\s*,https://addons.mozilla.org/en-CA/firefox/addon/,' \
	-e 's,$,/,'
)"

printf '\n%s\n\n' "$addons" >&2
{
	type firefox && firefox $addons
} > /dev/null 2>&1
