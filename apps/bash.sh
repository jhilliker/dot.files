#!/bin/sh

case "$(uname -s)" in
	MINGW*)  ;;
	CYGWIN*) ;;
	*)       echo "This script is for Windows only."; exit 1;
esac >&2

readonly bashPath='/c/Program Files/Git/bin/bash.exe'
readonly src='bash.exe.lnk'
readonly powerShellFindDesktopCmd="[Environment]::GetFolderPath('Desktop')"
readonly dst="$(cygpath -u "$(powershell -C "$powerShellFindDesktopCmd")")"

[ -e "$bashPath" ] || {
	printf "'%s' not found" "$bashPath"
	exit 1
}
[ -e "$src" ] || {
	printf "'%s' not found" "$src"
	exit 1
}

cp -vt "$dst" "$src"
