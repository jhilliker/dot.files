#!/bin/sh -e

##### dependencies ####################

real0="$(realpath -e "$0")"
scriptDir="${real0%/*}"
dotfilesDir="${scriptDir%%/dot.files*}/dot.files"
. "$dotfilesDir/lib/_util.sh"
. "$dotfilesDir/lib/colours.sh"
. "$dotfilesDir/lib/console.log.sh"
unset real0 #scriptDir dotfilesDir

[ "$USER" = "root" ] || fatal 'must run as root'
{ uname -a | grep -qi 'linux' ; } || fatal 'linux only'

isdefined sshd || fatal 'sshd not installed (openssh)'

##### settings #########################

readonly cfgDst='/etc/ssh/sshd_config'
readonly cfgSrc="$scriptDir/sshd.config"
[ -f "$cfgSrc" ] || fatal "'$cfgSrc' not found"

readonly customHeader='##### Local Customizations #####'

#### program ##########################

sedCmd="sed -n -e '
/$customHeader/ q;
/^\s*#/ {p;d};
/./! {p;d};
$(sed -E \
	-e 's/^\s*//' -e 's/\s*$//' -e 's/,$//' -e '/^#/d' -e '/./! d' \
	-e 's|^(\w+)\s+.*|/^\\s*\1\\b/ { s/^/#/ };|' \
	"$cfgSrc"
)
p
' '$cfgDst'"

log  "Using sed command:
$sedCmd
"

readonly config="$(
	eval $sedCmd \
	&& printf -- '%s\n\n' "$customHeader" \
	&& cat "$cfgSrc"
)" \
|| fatal

[ -n "$config" ] \
&& {
	printf -- '%s\n\n' "$config" \
	| tee "$cfgDst"
} \
&& systemctl enable sshd \
&& systemctl start sshd \
&& systemctl reload sshd \
|| fatal

systemctl status --no-pager sshd
echo

# advertise ssh server
[ ! -f /usr/share/doc/avahi/ssh.service ] \
|| cp /usr/share/doc/avahi/ssh.service /etc/avahi/services/

log WARN 'Add these rules:
# /etc/iptables/iptables.rules
# /etc/iptables/ip6tables.rules
# SSH
-A INPUT -p tcp -m tcp --dport 22 -m state --state NEW -j ACCEPT
'
