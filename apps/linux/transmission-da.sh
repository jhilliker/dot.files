#!/bin/sh -e

##### dependencies ####################

real0="$(realpath -e "$0")"
scriptDir="${real0%/*}"
dotfilesDir="${scriptDir%%/dot.files*}/dot.files"
. "$dotfilesDir/lib/_util.sh"
. "$dotfilesDir/lib/colours.sh"
. "$dotfilesDir/lib/console.log.sh"
unset real0 scriptDir dotfilesDir

[ "$USER" = "root" ] || fatal 'must run as root'
{ uname -a | grep -qi 'linux' ; } || fatal 'linux only'

checkInstalled() {
	for arg ; do
		isdefined "$arg" || fatal "'$arg' not installed"
	done
}
checkInstalled jq install realpath
unset checkInstalled

#######################################

readonly dldir='/home/transmission/Downloads'

readonly service='/usr/lib/systemd/system/transmission.service'

# See: https://github.com/transmission/transmission/blob/main/docs/Editing-Configuration-Files.md
readonly settings='/var/lib/transmission/.config/transmission-daemon/settings.json'
readonly sett='{
"alt-speed-time-enabled": true,
"alt-speed-time-begin": '$((8*60))',
"alt-speed-time-end": '$((21*60))',
"alt-speed-down": '$((2*1024))',
"alt-speed-up": 128,
"download-dir": "'$dldir'",
"incomplete-dir": "'$dldir'",
"peer-socket-tos": "lowcost",
"preallocation": 0,
"ratio-limit": 0.01,
"ratio-limit-enabled": true,
"seed-queue-enabled": true,
"seed-queue-size": 1,
"speed-limit-down": '$((8*1024))',
"speed-limit-down-enabled": true,
"speed-limit-up": 256,
"speed-limit-up-enabled": true,
"trash-original-torrent-files": true,
"umask": 2
}'

#######################################

isdefined transmission-cli && isdefined transmission-daemon || {
	log INFO 'Installing transmission-cli'
	yes | pacman -S transmission-cli
}

log INFO "Creating '$dldir'"
install -o transmission -g transmission -m775 -d "$dldir"
(
	cd '/home/transmission'
	ln -sfv -t . "$settings" "$service"
	chown --no-dereference transmission:transmission "${settings##*/}" "${service##*/}"
)

log INFO 'Set large network buffers for utp...'
(
	readonly dst='/etc/sysctl.d/60-transmission.conf'
	tmp="$(mktemp)" && trap "rm -f '$tmp'" EXIT
	cat > "$tmp" <<- EOF
		net.core.rmem_max = $(( 1024*1024 * 4 ))
		net.core.wmem_max = $(( 1024*1024 * 1 ))
	EOF
	if [ -e "$dst" ] ; then # don't clobber existing file
		# check if they're the same
		diff "$dst" "$tmp" \
		|| { log WARN "'$dst' exists but differs" ; sleep 1 ; }
	else
		install -m644 -T "$tmp" "$dst"
		log INFO '...restarting system configuration'
		sysctl --system
	fi
)

log INFO 'Installing & starting service daemon'
sed -e '/^Nice=/d' -e '/^ExecStart/i Nice=16' -i "$service"
systemctl daemon-reload # for above change in .service file
systemctl enable transmission.service
systemctl start  transmission.service

log INFO 'Changing daemon configuration'
until [ -f "$settings" ] ; do
	printf 'awaiting %s $i\r' "$settings" "$(date +%s | tail -c3)"
	sleep 1
done
(
	tmp="$(mktemp)" && trap "rm -f '$tmp'" EXIT
	printf -- %s "$sett" \
		| jq -s 'add' "$settings" - \
		| tee "$tmp"
	install -o transmission -g transmission -m664 --compare -b -T "$tmp" "$settings"
	systemctl reload transmission.service
)

log INFO
systemctl status --no-pager transmission.service

##### USER SETTINGS

log INFO "Adding user '$SUDO_USER' to transmission group"
usermod -a -G transmission "${SUDO_USER:?"missing or empty"}"

log INFO "Creating ~${SUDO_USER}/Downloads/torrents symlink"
(
	eval cd "~${SUDO_USER}/Downloads"
	if [ -e torrents ] ; then  # don't clobber existing file
		# check that it's already what we want
		[ "$(realpath -e torrents)" = "$dldir" ] \
		|| fatal "'$PWD/torrents' already exists"
	else
		ln -sfvT "$dldir" torrents
		chown --no-dereference ${SUDO_UID:?empty}:${SUDO_GID:?empty} torrents
	fi
)
