#!/bin/sh -e

##### dependencies ####################

real0="$(realpath -e "$0")"
scriptDir="${real0%/*}"
dotfilesDir="${scriptDir%%/dot.files*}/dot.files"
. "$dotfilesDir/lib/_util.sh"
. "$dotfilesDir/lib/colours.sh"
. "$dotfilesDir/lib/console.log.sh"
unset real0 #scriptDir dotfilesDir

[ "$USER" = "root" ] || fatal 'must run as root'
{ uname -a | grep -qi 'linux' ; } || fatal 'linux only'

isdefined iptables  || fatal 'iptables not installed'
isdefined ip6tables || fatal 'ip6tables not installed'

##### settings ########################

readonly cfgDst4='/etc/iptables/iptables.rules'
readonly cfgDst6='/etc/iptables/ip6tables.rules'
readonly cfgSrc="$scriptDir/iptables.rules"
[ -f "$cfgSrc" ] || fatal "'$cfgSrc' not found"

#######################################

# try to load the rules first, then install them
iptables-restore "$cfgSrc"
ip6tables-restore "$cfgSrc"

install --compare -m644 -vb -T "$cfgSrc" "$cfgDst4"
install --compare -m644 -vb -T "$cfgSrc" "$cfgDst6"
systemctl reload-or-restart iptables
systemctl reload-or-restart ip6tables
systemctl restart avahi-daemon

log INFO "IP4 Rules:"
iptables-save
log INFO "IP6 Rules:"
ip6tables-save

systemctl --no-pager status iptables ip6tables avahi-daemon
