#!/bin/sh -e

readonly ruleSRC='https://raw.githubusercontent.com/M0Rf30/android-udev-rules/master/51-android.rules'
readonly ruleDST='/etc/udev/rules.d/51-android.rules'

readonly grepStrings='
	https://developer.android.com/studio/run/device.html
	idVendor
	idProduct
	android_usb
	GROUP=
'

readonly tmp="$(mktemp)"
trap "rm -f '$tmp'" EXIT

curl -L "$ruleSRC" > "$tmp"
[ -s "$tmp" ]
# sanity check of d/l
for str in $grepStrings ; do
	grep -qF "$str" "$tmp" || exit 2
done

mv -f "$tmp" "$ruleDST"
chmod a+r "$ruleDST"
udevadm control --reload-rules
{ adb kill-server 2> /dev/null ; true ; }

readonly adbgroup="$(
	sed -En '/\bGROUP=/ {
		s/.*\bGROUP="(\w*)",.*/\1/
		p
		q
	}' "$ruleDST"
)"
[ -n "$adbgroup" ]
groupadd -f "$adbgroup"

echo
echo "User must be a member of the '$adbgroup' group"
echo "eg: sudo usermod -aG '$adbgroup' userLogin"
