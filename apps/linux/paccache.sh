#!/bin/sh
systemctl enable paccache.timer
systemctl start  paccache.timer

systemctl list-timers --all --no-pager
systemctl status paccache.timer --no-pager
systemctl status paccache.service --no-pager
