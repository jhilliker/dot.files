#!/bin/sh -e

##### dependencies ####################

real0="$(realpath -e "$0")"
scriptDir="${real0%/*}"
dotfilesDir="${scriptDir%%/dot.files*}/dot.files"
. "$dotfilesDir/lib/colours.sh"
. "$dotfilesDir/lib/console.log.sh"
unset real0 scriptDir dotfilesDir

##### settings ########################

readonly config="$(echo $HOME/.steam/steam/userdata/*/config/localconfig.vdf)"
log DEBUG "$config"
[ -f "$config" ] ||
	fatal "Unable to find config file"

tmp="$(mktemp)"
trap "rm -f '$tmp'" EXIT

##### program #########################

log INFO 'Setting: "LaunchOptions" "gamemoderun %command%"'

sed \
	-e '/"LaunchOptions"/d' \
	-e '/"Playtime"/a "LaunchOptions" "gamemoderun %command%"' \
	"$config" \
	> "$tmp"
[ -s "$tmp" ] ||
	fatal "Empty '$tmp'"
mv -bv "$tmp" "$config"
