#!/bin/sh -xe

type log > /dev/null 2>&1 || log() {
	printf '***** %s\n' "$*"
} >&2

fatal() {
	[ $# -gt 0 ] && log FATAL "$*"
	exit 1
}

isdefined() {
	type $1 >/dev/null 2>&1
}
[ "$USER" = "root" ] || fatal 'must run as root'
{ uname -a | grep -i 'linux' ; } || fatal 'linux only'

if isdefined apt-get ; then
	curl -sS 'https://download.spotify.com/debian/pubkey.gpg' | apt-key add -
	echo 'deb http://repository.spotify.com stable non-free' | tee /etc/apt/sources.list.d/spotify.list
	apt-get update
	apt-get install spotify-client
else
	fatal 'unsupported distro'
fi

log 'done'
exit 0
