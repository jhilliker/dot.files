// get ideas from
// https://github.com/arkenfox/user.js

user_pref("browser.proton.enabled", false);
user_pref("media.ffmpeg.vaapi.enabled", true);

user_pref("app.update.service.enabled", false);
user_pref("browser.discovery.enabled", false);
user_pref("browser.tabs.drawInTitlebar", true);
user_pref("layers.acceleration.force-enabled", true);

// privacy
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("extensions.pocket.enabled", false);
user_pref("network.dns.disablePrefetch", true);
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("network.predictor.cleaned-up", true);
user_pref("network.predictor.enabled", false);
user_pref("network.prefetch-next", false);

// new tab page
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.rows", 2);
user_pref("browser.newtabpage.activity-stream.showSponsored", false);
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false);
user_pref("browser.newtabpage.activity-stream.topSitesRows", 2);
user_pref("browser.newtabpage.pinned", "[{\"url\":\"https://mail.google.com\",\"label\":\"gmail\",\"customScreenshotURL\":\"https://upload.wikimedia.org/wikipedia/commons/2/2e/Gmail_2020.png\",\"baseDomain\":\"mail.google.com\"},{\"url\":\"https://drive.google.com/drive/my-drive\",\"label\":\"gdrive\",\"customScreenshotURL\":\"https://www.google.com/drive/static/images/drive/logo-drive.png\",\"baseDomain\":\"drive.google.com\"},{\"url\":\"https://gitlab.com/jhilliker\",\"label\":\"gitlab\",\"baseDomain\":\"gitlab.com\"},{\"url\":\"https://github.com/jhilliker\",\"label\":\"github\",\"baseDomain\":\"github.com\"},{\"url\":\"https://dillinger.io/\",\"label\":\"MarkDown\",\"customScreenshotURL\":\"https://markdown-here.com/img/icon256.png\"},{\"url\":\"https://www.photopea.com/\",\"label\":\"photopea\"},{\"url\":\"https://discord.com/channels/@me\",\"label\":\"discord\"},{\"url\":\"https://pubs.opengroup.org/onlinepubs/9699919799/\",\"label\":\"POSIX\"},{\"url\":\"https://libgen.rs/\",\"label\":\"libgen\"},{\"url\":\"https://annas-archive.org/\",\"label\":\"Anna’s Archive\"},{\"url\":\"https://thepiratebay.org/index.html\",\"label\":\"PirateBay\"},{\"url\":\"https://torrentgalaxy.to/\",\"label\":\"TorrentGalaxy\"},{\"url\":\"https://www.youtube.com/\",\"label\":\"YouTube\"},{\"url\":\"https://www.netflix.com/ca/login\",\"label\":\"Netflix\"},{\"url\":\"https://www.disneyplus.com/en-ca\",\"label\":\"Disney+\"}]");

// SSD stuff
user_pref("browser.cache.disk.enable", false);
user_pref("browser.cache.disk.capacity", 0);
user_pref("browser.cache.memory.enable", true);
user_pref("browser.sessionstore.interval", 120000); // 2*60*1000 = 2m // was 15s
