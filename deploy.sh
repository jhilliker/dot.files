#!/bin/sh -e

##### dependencies ########################################

readonly real0="$(realpath -se "$0")"
scriptDir="${real0%/*}"
readonly dotfilesDir="${scriptDir%%/dot.files*}/dot.files"
. "$dotfilesDir/lib/_util.sh"
. "$dotfilesDir/lib/colours.sh"
. "$dotfilesDir/lib/console.log.sh"
. "$dotfilesDir/lib/globmatch.sh"
log DEBUG "scriptDir: '$scriptDir'"

[ "$USER" != "root" ] || fatal 'dont run as root'

##### settings ############################################

readonly CP_cmd="cp -vbTp"
readonly LN_cmd="ln -vbTs"

unset deploy_cmd  # cmd to delpoy files: cp/ln (above). Selected by cmd line

exec >&2

##### arguments ###########################################

parseArgs() {
	local options="$(getopt -o 'cl' -l 'copy,link' -n "$0" -- "$@")"
	[ $? != 0 ] && fatal "Invalid arguments: $@"
	eval set -- "$options"
	unset options
	while : ; do
		case "$1" in
			-c|--copy)
				opt_copy='t'; shift ;;
			-l|--link)
				opt_link='t'; shift ;;
			--) shift; break ;;
			*) fatal "Invalid arguments: '$1'" ;;
		esac
	done
	[ $? != 0 ] && fatal "Invalid arguments: $*"

	[ -n "$opt_copy" ] && [ -n "$opt_link" ] \
		&& fatal "Invalid options. Can't both copy and link."
	readonly opt_copy opt_link
}
parseArgs "$@"

testLN() ( # subshell
	# determines if 'ln -s' will work
	log DEBUG "Testing $LN_cmd"

	readonly orig="tmp.$$.orig.tmp"
	readonly link="tmp.$$.link.tmp"

	cd "$(mktemp -d)" \
	&& trap "rm -rf '$PWD'" EXIT \
	&& echo "$$" > "$orig" \
	&& $LN_cmd "$orig" "$link" \
	&& [ -L "$link" ] \
	&& echo "$$ orig" >> "$orig" \
	&& echo "$$ link" >> "$link" \
	&& cmp -s "$orig" "$link"

	if [ $? -eq 0 ] ; then
		log DEBUG "'$LN_cmd' works"
	else
		log WARN "'$LN_cmd' does not work"
		return 1
	fi
)

if [ -n "$opt_copy" ] ; then
	deploy_cmd="$CP_cmd"
elif testLN ; then
	deploy_cmd="$LN_cmd"
else
	deploy_cmd="$CP_cmd";
fi
readonly deploy_cmd
[ -n "$opt_link" ] && [ "$deploy_cmd" != "$LN_cmd" ] && fatal 'Cannot create links'
log DEBUG "Using '$deploy_cmd'"

##### helpers #############################################

deploy() (  # subshell
	local src="$1"
	local dst="$2"

	log DEBUG "'$src' -> '$dst'"

	[ "$src" -ef "$dst" ] \
	&& {
		log DEBUG 'files are idenitical'
		return 0
	}

	: \
	&& src="$(realpath -se "$src")" \
	&& dst="$(realpath -s  "$dst")" \
	&& cd "${dst%/*}" \
	&& dst="${dst##*/}" \
	&& rm -vf "$dst" \
	&& src="$(realpath -se --relative-to=. --relative-base="$HOME" "$src")" \
	&& $deploy_cmd "$src" "$dst" \
	|| {
		log ERR 'failed to deploy'
		return 1
	}

	# if "copy", rewrite scripts to source their target
	if [ "$deploy_cmd" = "$CP_cmd" ] ; then
		if endsWith "$src" '.bash' ; then
			printf '#!/bin/bash\n. '\''%s'\''\n' "$src" | tee "$dst" \
			&& chmod u+x "$dst"
		elif endsWith "$src" '.sh' ; then
			printf '#!/bin/sh\n. '\''%s'\''\n' "$src" | tee "$dst" \
			&& chmod u+x "$dst"
		fi || {
			log ERR 'failed making redirect script'
			return 1
		}
	fi
	return 0
)

##### program #############################################

###########################################################
##### for files under $HOME
###########################################################

cd ~ || fatal 'failed cd to ~'
scriptDir="${scriptDir#$PWD/}"  # relative path

# use $HOME/dot.files if using ln
if [ "$deploy_cmd" = "$LN_cmd" ] ; then
	[ -e 'dot.files' ] \
	|| deploy "${scriptDir}" 'dot.files' \
	|| fatal
fi

if [ -e 'dot.files' ] ; then
	if [ 'dot.files' -ef "$scriptDir" ] ; then
		scriptDir='dot.files'
	else
		fatal "~/dot.files exists, but is not link to '${scriptDir}'"
	fi
fi

readonly scriptDir
log INFO "scriptDir: '$scriptDir'"

#######################################
##### ~/.*
#######################################
log INFO 'Deploying ~/.*'

status=0

for f in "$scriptDir/home/"* ; do
	dst="$f"
	dst="${dst%.bash}"
	dst=".${dst##*/}"
	deploy "$f" "$dst" || status=1
done

#######################################
##### ~/.config/*
#######################################
log INFO 'Deploying ~/.config/*'

mkdir -p .config || fatal

src="$scriptDir/config"
for f in "$src"/* ; do
	dst=".config/${f##*/}"
	deploy "$f" "$dst" || status=1
done


###########################################################
##### for files under /etc
###########################################################
cmp_v() {
	if [ ! -e "$1" ] && [ ! -e "$2" ] ; then
		log DEBUG "'$2' neither exist"
	elif [ ! -e "$1" ] ; then
		log WARN "'$1' doesnt exist"
	elif [ "$1" -ef "$2" ] ; then
		log DEBUG "'$2' are identical"
	elif cmp "$1" "$2" ; then
		log DEBUG "'$2' are the same"
	else
		log WARN "'$2' differ"
		diff -pdu --color=auto "$1" "$2" || true
		local targetDir="${2%/*}/"
		if [ -n "$targetDir" ] && [ ! -d "$targetDir" ] ; then
			printf -- "sudo mkdir -p '%s' && " "$targetDir"
		fi
		printf -- "sudo install -o root -m 644 -Cbv -t '%s' '%s'\n" "$targetDir" "$(realpath -e "$1")"
	fi
}

#######################################
##### /etc
#######################################
log INFO "Checking /etc"

src="$scriptDir/apps/linux/etc"
{ find "$src" -type f -print || fatal ;} \
| sort \
| while IFS= read -r f ; do
	dst="/etc/${f#$src/}"
	cmp_v "$f" "$dst"
done

#######################################
##### host specific
#######################################
: ${HOSTNAME=$(hostname)}
src="$scriptDir/apps/linux/$HOSTNAME"

log INFO "Checking ~ for '$HOSTNAME'"
{ find "$src/HOME" -type f -print ; } \
| sort \
| while IFS= read -r f ; do
	dst="$HOME${f#$src/HOME}"
	deploy "$f" "$dst" || status=1
done

log INFO "Checking /etc for '$HOSTNAME'"
{ find "$src/etc" -type f -print ; } \
| sort \
| while IFS= read -r f ; do
	dst="/${f#$src/}"
	cmp_v "$f" "$dst"
done

##### exit ################################################

if [ "$status" = '0' ] ; then
	log GREEN Done
	exit 0
else
	fatal
fi
