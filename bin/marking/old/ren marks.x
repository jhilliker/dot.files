for f in *.txt; do
	mark="$(sed -n '1p' < "$f" \
		| grep -Eo '\b[0-9.]{2,}\s*/\s*44\b' \
		| cut -d'/' -f1)";
	dst="$(echo "$f" | sed "s/ - / - $mark - /")"
	mv "$f" "$dst"
done

pullAssignment.sh -c 1280git.csv -d '2019-10-04 00:30' -i '**/a03/**' \
-x '**/books/**' -x '**/books.tar*' -x '**/pokemon.csv' -x '**/words.txt' -x '**/dir/**' \
-f a03cli.marks.txt

find a03/ -type d -name '.git' -execdir rm -rf {} \;
find a03/ -type f -name '*.sh' -delete
find a03/ -type f -name 'a03.q10.txt' -delete
find a03/ -type f -path '*/a03/*' -name '*' -execdir mv {} '..' \;
find a03/ -type d -name 'a03' -delete
grep -l -e '--------------------' a03/*/* | grep -v 'a03cli.marks.\w*.txt' | xargs -P1 -d'\n' sed -e '0,/--------------------/ d' -i



find co\ 2019-12-15\ 17h49m36s/ -type f \
| grep -v -e '.*log.txt' -e '.gitkeep' \
| while IFS= read f ; do

hash="$(echo "$f" | cut -d'/' -f2)"
dir="${f%/*}"
cat feed "$f" > "$dir/a02pipes.$hash.marks.txt" \
&& rm "$f"

done

