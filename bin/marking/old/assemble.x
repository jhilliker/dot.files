#!/bin/sh -e

files='
Part1_Inheritance.ts
Part2_DIP.ts
Part3_UML.*
'

srcFiles="$(readlink -f ../provided)"
dst="00_assembled"

assembleFile() {
	local readonly dstD="$1"
	local f="$2"
	local readonly dstF="$dstD/$f"
	rm -f "$dstF" "$dstF.[[:digit:]]*"
	local found="$(find ./a?? -type f -iname "$f" -print | sort)"
	[ -n "$found" ] || return 1
	[ $( echo "$found" | wc -l) -gt 1 ] && {
		diff $found || log WARN "multiple different files"
	}
	f="$(echo "$found" | tail -1)"
	cp -p "$f" "$dstF" && chmod a-w "$dstF" || {
		log ERR "Couldnt copy '$f'"
		return 1
	}
}

for d in * ; do
	[ -d "$d" ] || continue
	log "$d"
	(
		cd "$d" && rm -rf "$dst" && mkdir "$dst" || exit 0
		cp -pRt "$dst" "$srcFiles"/[!n]* "$srcFiles"/.[!.]*
		dst="$dst/src"
		ln "Part2 DIP scheme.$d.txt" "$dst/zz_Part2 DIP scheme.$d.txt"
		for f in $files ; do
			assembleFile "$dst" "$f"
		done
		dst="${dst%/*}"
		cd "$dst"
		ln -s "$srcFiles/node_modules"
	) || log ERR "$d"
done
