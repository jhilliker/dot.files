#!/bin/sh

readonly dstDir='marks'

readonly totalRE='^\s*\[\s*([.[:digit:]]+)\s*\/?\s*([[:digit:]]+)\s*\]\s*TOTAL'

readonly dstFileFmt='$lname.$fname._${raw:-XX}_.$stdno.$userid.${src##*/}'
readonly summaryFile='summary.txt'
marksGlob='*.marks.txt' # overriden by --marksGlob

type log > /dev/null 2>&1 || log() {
	echo "*** $*"
} >&2
fatal() {
	log FATAL "$*"
	exit 1
}

usage() {
	echo "$(basename $0) --dir checkoutDir [--marks GLOB] [--force]"
	[ "$#" -ne 0 ] && { echo ; log FATAL "$*" ; }
	exit 0
} >&2

parseArgs() {
	options="$(getopt \
		-o 'd:m:fh' -l 'dir:,marks:,force,help' \
		-n "$0" -- "$@")"
	[ $? = 0 ] || usage "Error in arguments"
	eval set -- "$options"
	unset options
	while : ; do
		case "$1" in
			-d|--dir)
				dir="$2" ; shift 2 ;;
			-m|--marks)
				marksGlob="$2" ; shift 2 ;;
			-f|--force)
				force='t' ; shift 1 ;;
			-h|--help)
				usage ;;
			--) shift; break ;;
			*)
				usage "Invalid arguments: '$o'";;
		esac
	done

	[ -d "$dir" ] || usage "--dir '$dir' is not a directory"
	readonly dir
}
parseArgs "$@"

getTotal() {
	sed -En "/$totalRE/ {
		s/$totalRE/\\1 \\2/
		p
		q
	}" "$1" \
	| awk -F' ' '{printf("raw=%s;outOf=%s;percent=%04.2f", $1, $2, 100*$1/$2);}'
}

cd "$dir" || fatal
[ -z "$force" ] || rm -rf "$dstDir"
mkdir "$dstDir" || fatal

grep -v '^#' \
| {
	unset i good noFeedback noTotal marks
	while IFS=',' read -r hash repo stdno userid fname lname rest ; do
		i=$(( i + 1 ))
		unset raw outOf percent totals src dst
		log INFO "#$i"

		src="$(find "$hash" -maxdepth 1 -type f -name "$hash.$marksGlob")"
		[ -f "$src" ] || "$(find "$hash" -maxdepth 1 -type f -name "$marksGlob")"

		if [ -f "$src" ] ; then
			totals="$(getTotal "$src")" || fatal
			if [ -n "$totals" ] ; then
				eval "$totals" # raw outOf percent
				good=$(( good + 1 ))
				marks="$marks$percent " # a list of marks

			else
				noTotal="$noTotal$hash "
				log WARN "No total: $hash"
			fi

			eval dst="\"\$dstDir/$dstFileFmt\""
			{
				cat <<- EOF
					First: $fname
					Last: $lname
					Student ID: $stdno
					User ID: $userid
					Repo: $repo
					----------------------------------------
				EOF
				cat "$src"
			} > "$dst" || fatal
		else
			noFeedback="$noFeedback$hash "
			log WARN "No feedback: $hash $userid $fname $lname"
		fi
	done

	log INFO "$good / $i good"

	printf '%s' "$marks" | tr ' ' '\n' | sort -n | awk '
		BEGIN { printf("\nmrks="); }
		$1 != "" {
			printf("%04.2f ", $1);
			cnt++;
			sum += $1;
			sumSq += $1 ** 2;

			tens = int($1 / 10);
			ones = int($1 - tens * 10);
			marks[tens, "cnt"]++;
			marks[tens, ones]++;
		}

		END {
			printf("\n\n")
			for(i=0; i <= 10; i++) {
				printf("%2d0s n=%02d: ", i, marks[i, "cnt"]);
				for(j=0; j <= 9; j++) {
					for(c=0; c < marks[i, j]; c++) printf("%d", j);
				}
				printf("\n");
			}
			printf("\n");
			avg = sum / cnt;
			std = sqrt( sumSq / cnt - avg ** 2 );
			printf("cnt=%d\n", cnt);
			printf("avg=%04.2f\n", avg);
			printf("std=%04.2f\n", std);
			printf("\n");

			cnt -= marks[0, 0];
			avg = sum / cnt;
			std = sqrt( sumSq / cnt - avg ** 2 );
			printf("X 00s (n=%d)\n", marks[0, 0]);
			printf("cnt=%d\n", cnt);
			printf("avg=%04.2f\n", avg);
			printf("std=%04.2f\n", std);
			printf("\n");

		}' | tee "$dstDir/$summaryFile"

	[ -z "$noFeedback" ] || log WARN "No feedback:"$'\n'"$noFeedback"
	[ -z "$noTotal" ] || log WARN "No total:"$'\n'"$noTotal"

	[ -z "$noTotal" ] && [ -z "$feedback" ] # exit status
}
