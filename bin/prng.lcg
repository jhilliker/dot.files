#!/bin/sh -e

##### dependencies ########################################

real0="$(realpath -e "$0")"
scriptDir="${real0%/*}"
dotfilesDir="${scriptDir%%/dot.files*}/dot.files"
. "$dotfilesDir/lib/_util.sh"
. "$dotfilesDir/lib/colours.sh"
. "$dotfilesDir/lib/console.log.sh"
unset real0 scriptDir dotfilesDir

##### args ############################

usage() {
local exe="${0##*/}"
cat << USAGE
NAME
    $exe - generates random numbers
SYNOPSIS
    $exe [OPTIONS]...
DESCRIPTION
    Genertes a stream of random numbers between 0 and 2^32 -1.
    Seeds with current time unless overridden.
    Command line options override environment variables.
OPTIONS
    -h, --help
        Displays the help message.
    -b BOUND, --bound BOUND
        The upper bound of generated numbers, exclusive.
        All numbers will be less than this.
    -s SEED, --seed SEED
        The starting seed for the random number generator.
        Using the same starting seed will generate the same sequence.
ENVIRONMENT VARIABLES
    PRNG_BOUND
        Same as the -b option.
    PRNG_SEED
        Same as the -s option.
EXAMPLE USAGE
    $exe
    $exe -b 1024 -s 19991231
    PRNG_BOUND=1024 PRNG_SEED=19991231 $exe
USAGE

	[ $# -eq 0 ] || fatal "$@"
	exit 0
} >&2

parseArgs() {
	unset optBound optSeed

	# allArgs="$@"
	local options # separate line so $? works
	options="$(getopt \
		-o 'hb:s:' -l 'help,bound:,seed:' \
		-n "${0##*/}" -- "$@")"
	[ $? -eq 0 ] || fatal getopt
	eval set -- "$options"
	unset options
	while : ; do
		case "$1" in
			-h|--help)
				usage    # exits
				;;
			-b|--bound)
				optBound="$2"
				shift 2
				;;
			-s|--seed)
				optSeed="$2"
				shift 2
				;;
			--)
				shift; break ;;
			*)
				fatal "Invalid option: '$1'" ;;
		esac
	done
	[ $# -eq 0 ] || fatal "Illegal arguments: $*"

	readonly optBound
	readonly optSeed

	unset bound
	: ${bound:="$optBound"}
	: ${bound:="$PRNG_BOUND"}
	[ -z "$bound" ] || {
		util_isInteger "$bound" || fatal "Illegal number: bound=$bound"
		[ "$bound" -gt 0 ] || fatal "Illegal number: bound=$bound must be positive"
	}
	readonly bound

	unset seed
	: ${seed:="$optSeed"}
	: ${seed:="$PRNG_SEED"}
	: ${seed:=$(date +%s%N)}
	util_isInteger "$seed" || fatal "Illegal number: seed=$seed"

} >&2
parseArgs "$@"
unset parseArgs usage

##### program #########################

if [ -z "$bound" ] ; then
	print() {
		printf '%u\n' $(( seed >> 16 ))
	}
else
	print() {
		# TODO: make uniform
		# see: https://github.com/openjdk/jdk/blob/master/src/java.base/share/classes/java/util/Random.java
		printf '%u\n' $(( (seed >> 16) % bound ))
	}
fi

next() {
	# https://en.wikipedia.org/wiki/Linear_congruential_generator
	# C90, C99, C11: Suggestion in the ISO/IEC 9899, C18
	# seed=$(( ( 1103515245 * seed + 12345 ) & 2147483647 )) #2^31-1

	# java, POSIX
	# m=2^48, a=25214903917, c=11
	seed=$(( ( 25214903917 * seed + 11 ) & 281474976710655 )) #2^48-1
}

run() {
	while : ; do
		next
		print
	done
}

run
