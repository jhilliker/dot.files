#!/bin/sh -e

[ $# -gt 0 ] || {
	cat <<- EOF
	${0##*/}: no PIDs specified
	Try '${0##*/} --help' for more information.
	EOF
	exit 1
} >&2

[ "$1" != '-h' ] && [ "$1" != '--help' ] && [ "$1" != '-?' ] || {
cat << EOF
Prints the given PIDs, and all of their descendants' PIDs, one per line.
Usage:
	${0##*/} PID...
eg:
	${0##*/} $PPID
	${0##*/} $PPID $$
	${0##*/} \$\$
	${0##*/} \$(pgrep bash)
EOF
exit 0
}

getallchildren() {
	[ $# -gt 0 ] || return 0
	printf -- '%u\n' "$@" || return 2
	[ $# -lt 2 ] || {
		set -- "$(printf -- '%u,' "$@")"  # 1 2 3 -> 1,2,3,
		set -- "${1%,}"                   # 1,2,3, -> 1,2,3
	}
	getallchildren $(ps -o pid= --ppid $1)
}

children="$(getallchildren "$@")" || exit $?
# ^ contains duplicates when arguments contain an ancestor/descendant relation
printf -- %s "$children" | sort -nu  # remove duplicates
