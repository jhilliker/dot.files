//import { Application, Request, Response, NextFunction } from 'express';

import toNumArray from './sum.toNumArray';

describe('sum app', () => {
  describe('toNumArray', () => {
    ['[1,2,3]', '1+2+3', ['1', '2', '3']].forEach(v => {
      test(`${v}`, () => {
        expect(toNumArray(v)).toMatchObject([1, 2, 3]);
      });
    });

    test('undefined', () => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      expect(() => toNumArray((undefined as any) as string)).toThrow();
    });
  });

  // TODO path?
});
