import { Application, Request, Response, NextFunction } from 'express';
import { check } from 'express-validator';

import toNumArray from './sum.toNumArray';
import sumFn from './sum.fn';

function fn(app: Application, path: string): void {
  const sanitizer = check('nums').customSanitizer(v => toNumArray(v, path));

  app.get(
    path,
    sanitizer,
    (req: Request, res: Response) => {
      res.status(200).send({ sum: sumFn(...req.query.nums) });
    },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (err: any, req: Request, res: Response, next: NextFunction) => {
      res.status(400).send(err);
      next(err);
    }
  );
}

export default async function(
  app: Application,
  path: string
): Promise<Application> {
  return new Promise((resolve /*, reject*/) => {
    fn(app, path);
    resolve(app);
  });
}
