import express from 'express';

/* eslint-disable @typescript-eslint/no-unused-vars */
import sumApp from './sum/sum.app';
/* eslint-enable @typescript-eslint/no-unused-vars */

const app = express();

sumApp(app, '/sum').then(() => {
  const server = app.listen(process.env.PORT || 8080);
  server.on('listening', () => {
    console.log(server.address());
  });
});
