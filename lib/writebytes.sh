
# from https://www.etalabs.net/sh_tricks.html

writebytes() {
	printf %b $(printf \\\\%03o "$@")
}

writeoct () {
	printf %b $(printf \\\\%s "$@")
}

writeoct2 () {
	printf %b $(printf \\%03o $(printf 0%s\  "$@"))
}
