
isdefined() {
	type "$1" > /dev/null 2>&1
}

isdefined isinteractive || isinteractive() {
	[ -n "$PS1" ]
}

# basic logging
isdefined log || log() {
	printf -- '*** %s\n' "$*"
} >&2
isdefined fatal || fatal() {
	log FATAL "$@"
	exit 1
}

export readonly nl='
'

trim() {
	sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'
}

util_IFS_show() {
	printf -- '%s' "$IFS" | od -abc
}

util_isInteger() {
	[ $1 -ge 0 ] || [ $1 -le 0 ]
} 2>/dev/null

util_getLongestLineLength() {
	if [ $# -eq 0 ] ; then
		wc -L # read from stdin
	else
		printf -- '%s\n' "$@" | wc -L
	fi
}

# exportfunctions() {
# 	if ( foo() { : ; } ; export -f foo ) > /dev/null 2>&1 ; then
# 		export -f "$@"
# 	fi
# }
