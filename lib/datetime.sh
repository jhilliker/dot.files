
dt_S=1
dt_M=$(( 60 * dt_S ))
dt_H=$(( 60 * dt_M ))
dt_D=$(( 24 * dt_H ))
dt_W=$((  7 * dt_D ))

dt_getNowEpoch() {
	date +%s
}

dt_getLastModifiedEpoch() {
	stat -c %Y "$1"
}

dt_getAge_Epoch_r() {
	# $1 date being examined
	retVal=$(( "$(dt_getNowEpoch)" - "$1" ))
}

dt_isFresh_Epoch() {
	# $1 date being examined
	# $2 tooOld
	# missing things are stale
	dt_getAge_Epoch_r "$1" \
	&& [ "$retVal" -lt "$2" ]
}

dt_isStale_Epoch() {
	! dt_isFresh_Epoch "$@"
}

# files that dont exist are older

dt_getAge_File_r() {
	# $1 file to check
	local when= # two lines so $? works (local breaks it)
	when="$(dt_getLastModifiedEpoch "$1")" \
	&& dt_getAge_Epoch_r "$when"
}

dt_isFresh_File() {
	# $1 file to check
	# $2 too old
	local when=
	when="$(dt_getLastModifiedEpoch "$1")" \
	&& dt_isFresh_Epoch "$when" "$2"
}

dt_isStale_File() {
	# $1 file to check
	# $2 too old
	! dt_isFresh_File "$@"
}

dt_secondsToIso8601duration_r() {
	local in="$1"

	if [ "$in" -gt 0 ] ; then
		: # sanity check
	elif [ "$in" -lt 0 ] ; then
		in=$(( - in ))
	elif [ "$in" -eq 0 ] ; then
		retVal='PT0S'
		return 0
	else
		# wasnt a number !
		return 2
	fi 2>/dev/null

	retVal='P'
	local unit unitLen unitQuant
	for unit in W D T H M S ; do
		[ $in -gt 0 ] || break
		if [ "$unit" = T ] ; then
			unitQuant=
		else
			eval unitLen="\$dt_${unit}"
			if [ "$unitLen" -le "$in" ] ; then
				unitQuant=$(( in / unitLen ))
				in=$(( in - unitQuant * unitLen ))
			else
				continue # dont add this to string
			fi
		fi
		retVal="$retVal$unitQuant$unit"
	done
}
