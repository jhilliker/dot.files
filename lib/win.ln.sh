
# only load these functions on windows
case "$(uname -s)" in
	MINGW*)  ;;
	CYGWIN*) ;;
	*)	return 0
esac

___win_ln_powershell_isAdmin() {
	powershell -Command '
		$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent([Security.Principal.TokenAccessLevels]"Query,Duplicate"))
		$isAdmin = $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
		exit ! $isAdmin
	'
}

___win_ln_powershell_command_asAdmin() {
	powershell -Command '&{
		$p = New-Object System.Diagnostics.Process
		$p.StartInfo.FileName = Get-Command powershell
		#$p.StartInfo.RedirectStandardError = $true
		#$p.StartInfo.RedirectStandardOutput = $true
		$p.StartInfo.UseShellExecute = $true
		$p.StartInfo.Arguments = "'"$1"'"
		$p.StartInfo.Verb = "RunAs"
		$p.Start() | Out-Null
		#Do Other Stuff Here....
		$p.WaitForExit()
		exit $p.ExitCode
	}'
}

___win_ln_powershell_NewItem() {
	local itemType="$1"
	local target="$(cygpath -w "$2")"
	local linkname="$(cygpath -w "$3")"
	___win_ln_powershell_command_asAdmin \
		"&{
			cd '$(cygpath -w "$PWD")'
			echo '1: $1'
			echo '2: $2'
			echo '3: $3'
			echo 'PWD: \$PWD'
			echo 'Target: $target'
			echo 'Linkname: $linkname'
			echo 'ItemType: $itemType'
			New-Item -ItemType $itemType -Name '$linkname' -Target '$target'
			if ( ! \`\$?\` ) { pause; exit 1; }
		}"
}

ln() {
	## FIXME doesnt work if target is a directory
	local optName=""
	local opt_s=""
	local opt_v=""
	local opt_b=""
	local opt_T=""
	local OPTIND=""
	while getopts svbT optName; do
		case "$optName" in
			s)	opt_s="s";; #symlink
			v)	opt_v="v";; #verbose
			b)	opt_b="b";; #backup
			T)	opt_T="T";; #target not dir
			*)	log ERR "Invalid option: $optName" ; return 1;; ## TODO add t option
		esac
	done
	shift $(( OPTIND - 1 ))

	local target="$1";
	local linkname="$2";
	[ -n "$linkname" ] || linkname="${target##*/}"

	[ -e "$target" ] || { log ERR "'$target' does not exist" ; return 1; }

	[ -n "$opt_T" ] && [ -d "$linkname" ] \
		&& { log ERR "'$linkname' is a directory"; return 1; }

	local itemType
	if [ -n "$opt_s" ] ; then itemType='SymbolicLink'
	elif [ -d "$target" ]; then itemType='Junction'
	elif [ -f "$target" ]; then itemType='HardLink'
	else
		log ERR "unknown file type: '$target'"
		stat "$target"
		return 1
	fi

	if [ -e "$linkname" ] && [ -n "$opt_b" ] ; then # make backup
		if [ -n "$opt_v" ]
		then mv -v -- "$linkname" "$linkname~" || return 1
		else mv -- "$linkname" "$linkname~" || return 1
		fi
	fi
	rm -f -- "$linkname" || return 1

	[ -n "$opt_v" ] && echo "'$linkname' ~> '$target'"
	___win_ln_powershell_NewItem "$itemType" "$target" "$linkname"
	[ -n "$opt_v" ] && ls -l "$linkname"
} >&2
