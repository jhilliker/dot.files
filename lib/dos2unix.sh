
isdefined dos2unix || {
	dos2unix() {
		if [ "$#" -eq '0' ] ;	then
			sed 's/\r$//'
		else
			sed 's/\r$//' -i "$@"
		fi
	}
}

dos2unix-all() {
	find -name '.git' -prune -o -type f -print0 \
		| xargs -tr0 dos2unix
}
