
# colours?
[ -n "$COLORTERM" ] || \
case "$TERM" in
	xterm-color|*-256color)
		COLORTERM="$TERM"
		;;
	*)
		[ -x /usr/bin/tput ] \
		&& tput setaf 1 > /dev/null 2>&1 \
		&& COLORTERM=t
		;;
esac

# see
# https://en.wikipedia.org/wiki/ANSI_escape_code#3/4_bit

export CSI="$(printf -- '%b[' '\033')"  # Control Sequence Introducer: ESC[

___clr_colors_setColors() {
	local readonly colors='BLACK RED GREEN YELLOW BLUE MAGENTA CYAN WHITE'
	local i=0
	local c=
	for c in $colors ; do
		export      FG_$c="${CSI}3${i}m"
		export FG_${c}_BR="${CSI}3${i};9${i}m"
		export      BG_$c="${CSI}4${i}m"
		export BG_${c}_BR="${CSI}4${i};10${i}m"
		i=$(( i + 1 ))
	done
}
___clr_colors_setColors \
&& unset ___clr_colors_setColors

___clr_colors_setStyles() {
	local readonly STYLES='RESET BOLD FAINT ITALIC UNDERLINE BLINK_SLOW BLINK_FAST REVERSE CONCEAL CROSSED_OUT'
	local i=0
	local s=
	for s in $STYLES ; do
		export STYLE_${s}="${CSI}${i}m"
		i=$(( i + 1 ))
	done
}
___clr_colors_setStyles \
&& unset ___clr_colors_setStyles

export RST="$STYLE_RESET"

colorsShow() {
	printf '%b' "$RST"
	for bg in $( set | grep -o '^BG_[_[:upper:]]*' ); do
		eval printf '%8.8s%b' "${bg#BG_}" "\"\$$bg\""
		for fg in $( set | grep -o '^FG_[_[:upper:]]*' ); do
			eval printf '%b%-4.4s' "\"\$$fg\"" "'${fg#FG_}'"
		done
		eval printf \''%b%b%8.8s%b\n'\' "'$RST'" "\"\$FG_${bg#BG_}\"" "'${bg#BG_}'" "'$RST'"
	done
}

stylesShow() {
	printf "$RST"
	for style in $( set | grep -o '^STYLE_[_[:upper:]]*' ); do
		printf '%12.12s' "${style#STYLE_}"
		eval printf "\" \$$style\""
		printf '%-12.12s' "${style#STYLE_}"
		printf '%s\n' "$RST"
	done
}
