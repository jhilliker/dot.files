
read_pattern() {
	#1 RE to match
	#2 prompt message
	local RE="$1"
	shift
	until {
		[ -z "$1" ] || printf -- '%s' "$@"
		IFS= read -r REPLY
		printf -- %s "$REPLY" | grep -qP "$RE" \
		|| { printf -- 'Invalid entry\n' ; false ;}
	} ; do : ; done
} >&2

read_email() {
	# http://emailregex.com/
	local RE='(?:[a-z0-9"(?:[a-z0-9%&'\''*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'\''*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])'
	read_pattern "$RE" "$@"
}

read_yn() {
	local RE='^[yn]$'
	read_pattern "$RE" "$@" "[y/n] "
}
