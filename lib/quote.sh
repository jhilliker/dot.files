
quoteStdin_n() {
	# quote each line of stdin
	local q="${1:-"'"}"
	sed "s/$q/$q\\\\$q$q/g; s/^/$q/; s/\$/$q/"
}

quoteStdin_0() {
	# quote each null-terminated string
	local q="${1:-"'"}"
	sed -z "s/$q/$q\\\\$q$q/g; s/^/$q/; s/\$/$q/"
}

quoteStdin_1() {
	# quote stdin as one multi-line string
	local q="${1:-"'"}"
	sed "s/$q/$q\\\\$q$q/g; 1s/^/$q/; \$s/\$/$q/"
}

isdefined contains || contains() {
	[ -z "${2##*$1*}" ]
}

quote() {
	local quotechar="$2"
	case "$quotechar" in
		\') ;;
		\") ;;
		 *)
			if contains \' "$1" && ! contains \" "$1" ; then
				quotechar=\"
			else
				quotechar=\'
			fi
			;;
	esac

	printf -- '%s' "$1" | quote-stdin-1 "$quotechar"
}
