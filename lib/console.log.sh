
# logger -e --id -p user.err -s -t bash -- "a mesg2"
# systemd-cat
# journalctl -f -o short-iso --no-hostname -S now --user -t bash

[ -n "$LOG_LEVEL" ] \
|| export LOG_LEVEL='DEBUG'

___log_getLogLevel_info() {
	___log_info_name="$1"
	___log_info_num=
	___log_info_clr=
	case "$___log_info_name" in
		'') ;;
		DEBUG)
			___log_info_num=100
			___log_info_clr="$FG_WHITE"
			;;
		INFO)
			___log_info_num=200
			___log_info_clr="$FG_YELLOW"
			;;
		WARN|WARNING)
			___log_info_num=300
			___log_info_clr="$FG_MAGENTA"
			;;
		ERR|ERROR)
			___log_info_num=400
			___log_info_clr="$FG_RED"
			;;
		FATAL)
			___log_info_num=500
			___log_info_clr="$FG_RED_BR"
			;;
		*)
			___log_getLogLevel_info_ "$1"
	esac
}

eval "$(env | sed -E \
	-e '1i ___log_getLogLevel_info_() {\ncase "$1" in' \
	-e '$a *) return 1 ;;\nesac\n}' \
	-e '/^FG_\w+/! d' \
	-e 's/^FG_(\w+)=.*/\1|FG_\1) ___log_info_num=600; ___log_info_clr=$FG_\1 ;;/'
)"

log() {
	local ___log_info_name=
	local ___log_info_num=
	local ___log_info_clr=

	___log_getLogLevel_info $LOG_LEVEL \
	|| 	___log_getLogLevel_info DEBUG
	local ___log_filter_num="$___log_info_num"

	# find our info
	if ___log_getLogLevel_info "$1" ; then
		shift
	else
		___log_getLogLevel_info INFO
	fi

	# should we log
	[ "$___log_info_num" -ge "$___log_filter_num" ] || return 0

	if [ -n "$COLORTERM" ] && [ -t 1 ] ; then
		local fmtStr="${RST}%s ${___log_info_clr}%s %s${RST}"
	else
		local fmtStr="%s %s %s"
	fi

	if [ -n "$BASH_VERSION" ] && [ -n "${BASH_LINENO[0]}" ]; then # ln# in caller
		___log_info_name="$___log_info_name (${BASH_SOURCE[1]##*/}:${BASH_LINENO[0]})"
	fi

	printf "$fmtStr\n" "$(date --rfc-3339=s)" "$___log_info_name" "$*"
} >&2

fatal() {
	log FATAL "$@"
	exit 1
}
