
git_getTop() {
	# needs pwd in repo
	git rev-parse --show-toplevel
}

git_setCdToTop() {
	local top  # different line so $? works (local breaks it)
	[ -d "$1" ] || set "$(dirname "$1")"
	cd "$1" \
	&& cd "${PWD%/.git}" \
	&& top="$(git_getTop 2>/dev/null)" \
	&& cd "$top"
}

git_isFetchStale() {
	# needs $maxAge
	# needs pwd at repo's top
	dt_isStale_File '.git/FETCH_HEAD' "${maxAge:-0}"
}

git_getLastFetchEpoch() {
	# needs pwd at repo's top
	# result on stdout
	dt_getLastModifiedEpoch '.git/FETCH_HEAD'
}

git_getLastFetchAge_r() {
	# needs pwd at repo's top
	# result in retVal
	dt_getAge_File_r '.git/FETCH_HEAD'
}
