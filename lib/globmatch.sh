
globmatch() {
	[ $# -eq 2 ] || {
		cat <<- EOF
			Usage: globmatch glob string
			eg:
			globmatch "?p*" apple    -> true
			globmatch "?r*" apple    -> false
		EOF
		return 2
	}
	[ -z "${2%%$1}" ]
	# case "$2" in
	# 	$1) return 0 ;;
	# esac
	# return 1
}

endswith() {
	[ $# -eq 2 ] || {
		cat <<- EOF
			USAGE: endswith glob string
			eg:
			startswith a apple
			startswith '[[:lower:]]' apple
		EOF
		return 2
	} >&2
	globmatch "*$1" "$2"
}

startswith() {
	[ $# -eq 2 ] || {
		cat <<- EOF
			USAGE: startswith glob string
			eg:
			startswith a apple
			startswith '[[:lower:]]' apple
		EOF
		return 2
	} >&2
	[ "$2" != "${2##$1}" ] || [ -z "$1" ]
}

contains() {
	[ $# -eq 2 ] || {
		cat <<- EOF
			USAGE: contains glob string
			eg:
			contains p apple
			contains '???' apple
		EOF
		return 2
	} >&2
	startswith "*$1" "$2"
}

# TODO BUG
# x='apple\'
# y='*\'
# z="${x%%$y}"
# echo "$z"
