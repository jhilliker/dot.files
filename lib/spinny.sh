
spinny() {
	printf '%s' "$1"
	(
		trap 'printf "${1+done.\n}"; exit 0' TERM
		while : ; do
			for c in '/' '-' '\' '|' ; do
				printf '%s\b' "$c"
				sleep 1 || break 2
			done
		done
	) &
	SPINNY=$!
}
