
___sourceall_one() {
	. "$1"
}

# $SOURCEALL_TIMES enables debug load time records
[ -z "$SOURCEALL_TIMES" ] \
|| ___sourceall_one() {
	local status=0
	local start=$(date +%s%N)
	. "$1" || status=1
	SOURCEALL_TIMES="$SOURCEALL_TIMES
	$(( $(date +%s%N) - start )) $1"
	return $status
}

___sourceall() {
	local status=0
	local f
	for f in "$@" ; do
		[ -f "$f" ] || continue
		___sourceall_one "$f" || {
			log ERROR "$f"
			status=1
		}
	done
	return $status
}
