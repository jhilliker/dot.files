#!/bin/bash

# don't search path for sourced files
shopt -u sourcepath

DOTFILES_DIR="$(realpath "${BASH_SOURCE[0]}")"
readonly DOTFILES_DIR="${DOTFILES_DIR%/home/*}"

.files() {
	#SOURCEALL_TIMES=$(date +%s%N)  # debug load times
	. "$DOTFILES_DIR/lib/.sourceall.sh" \
	&& ___sourceall \
		"$DOTFILES_DIR/lib/_util.sh" \
		"$DOTFILES_DIR/lib"/* \
		"$DOTFILES_DIR/profile.d"/* \
	|| return 1

	isinteractive || return 0
	___sourceall "$DOTFILES_DIR/profile.d/interactive"/* || return 1

	[ -z "$SOURCEALL_TIMES" ] \
	|| {
		local start=${SOURCEALL_TIMES%%[[:space:]]*}
		local totalNS=$(( $(date +%s%N) - start ))
		echo "${SOURCEALL_TIMES#*[[:space:]]}"
		printf -- '\t%d ns = %f s\n' "$totalNS" "$(echo "scale=9;$totalNS * 10^-9" | bc)"
	}
}
.files
