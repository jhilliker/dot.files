
export TZ='America/Vancouver'

[ "$XDG_SESSION_TYPE" != 'wayland' ] || export MOZ_ENABLE_WAYLAND=1
