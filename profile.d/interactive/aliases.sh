# basic aliases
alias cls='clear'
alias grep='grep --color=auto'
alias egrep='grep -E --color=auto'
alias diff='diff -pdu --color=auto'
alias ls='ls -Fhb --color=auto'
alias ll='ls -Fhbl --color=auto'
alias la='ls -Fhbla --color=auto'
alias l.='ls -Fhbd --color=auto .*'
alias l='ls -lA --color=auto'
alias md='mkdir'
alias rd='rm -r'
alias rmrf='rm -rf'
alias rm-rf='rm -rf'
alias watch='watch --color -x'
alias f='find . -name'
alias df='df -hT'
alias du='du -h'
alias dd='dd status=progress'
alias free='free -h'
alias sudo='sudo -v; sudo '  # refresh sudo timestamp on each use

# aliases for things that might not be installed.  Check first.
! isdefined ip || ! ip -h 2>&1 | grep -q -- '\s-c\[olor\]' || alias ip='ip -c'
! isdefined git || { alias g='git'; alias gs='git status'; }
! isdefined gamemoderun || ! isdefined obs || alias obs='gamemoderun obs'
! isdefined asciiquarium || isdefined aquarium || alias aquarium='asciiquarium'
! isdefined cmatrix || isdefined matrix || alias matrix='cmatrix'

# nices
# fg^-_  bg^-_    batch^-_  idle
# 2,4,6  8,10,12  14,16,18  19
! isdefined firefox  || alias firefox='nice -n2  firefox'
! isdefined scrcpy   || alias  scrcpy='nice -n4  scrcpy'
! isdefined pacman   || alias  pacman='nice -n6  pacman'
! isdefined yay      || alias     yay='nice -n10 yay'
! isdefined top      || alias     top='nice -n12 top'
! isdefined htop     || alias    htop='nice -n12 htop'
! isdefined btop     || alias    btop='nice -n12 btop'

# custom commands
alias  iptables-list='iptables  -L -n -v --line-numbers'
alias ip6tables-list='ip6tables -L -n -v --line-numbers'
alias rot13='tr A-Za-z N-ZA-Mn-za-m'
alias memsort='free -h && echo && ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head'
alias zonk='ps -u $USER | grep -Ev "ssh|ps|grep|cut|xargs|kill|bash" | cut -d" " -f2 | xargs -r kill'
! isdefined ffprobe || alias fflength='ffprobe -v error -sexagesimal -select_streams v:0 -show_entries stream=duration -show_entries stream_tags=duration -of default=noprint_wrappers=1:nokey=1 -i'
! isdefined pacman || alias orphans='pacman -Qtdq'
! isdefined pacman || alias removeorphans='sh -c '\''
	if [ $(pacman -Qtdq | wc -l) -gt 0 ]
	then   pacman -Qtdq | grep -v -- 'debug' | pacman -Rns -
	else echo No orphans to remove >&2 ; false
	fi'\'''

# Add an "alert" alias for long running commands. Use like so: sleep 10; alert
! isdefined notify-send \
|| alias alert='notify-send --urgency=low -i \
	"$([ $? = 0 ] && echo terminal || echo error)" \
	"$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# web apps
alias ada='curl rate.sx/ada'
alias myip='curl ip.me'
alias weather='curl -sS wttr.in | head -n -2'
alias moon='curl -sS wttr.in/Moon | head -n -2'

# cd rewrite aliases
# ... -> cd ./../../
# cd... -> SAA
___alias_src='.'
___alias_dst='cd ./'
for ___alias_i in $(seq 9) ; do
	___alias_src="${___alias_src}."
	___alias_dst="${___alias_dst}../"
	alias "${___alias_src}"="${___alias_dst}"
	alias "cd${___alias_src}"="${___alias_dst}"
	alias ".$___alias_i"="${___alias_dst}"
done
unset ___alias_src ___alias_dst ___alias_i
