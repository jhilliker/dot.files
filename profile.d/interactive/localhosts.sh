
___getlocaladdr() {
	local TIMEOUT='1s'
	local addr="$1.local"
	unset ___localaddr
	: ${___localaddr:=$(echo "trying hostent" >&2
		timeout "$TIMEOUT" getent hosts "$addr" | cut -d" " -f1)}
	: ${___localaddr:=$(echo "trying avahi-resolve" >&2
		timeout "$TIMEOUT" avahi-resolve -4n "$addr" | cut -f2)}
	: ${___localaddr:=$(echo "trying known_hosts" >&2
		sed -En "/^$addr,/ { s/^[^,]+,// ; s/\s.*// ; p }" < ~/.ssh/known_hosts)}
	[ -z "$2" ] || : ${___localaddr:=$(echo "trying static $2" >&2
		printf -- '%s' "$2")}
	[ -n "$___localaddr" ] && printf 'using %s\n' "$___localaddr" >&2
}

# ssh hostnames
alias acer3rd='___getlocaladdr acer3rd && ssh "$___localaddr"'
alias dv5='___getlocaladdr dv5 "192.168.0.104" && ssh "$___localaddr" -L 9091:localhost:9091'
alias y460='___getlocaladdr y460 "192.168.0.108" && ssh "$___localaddr"'
alias a5pro='adb connect 192.168.0.110 && nice -n4 scrcpy --stay-awake --max-size 1140 --max-fps 15 --bit-rate 4M'
