
___prompt_load_liquid() {
	local src=
	for src in \
		"$(which liquidprompt 2> /dev/null)" \
		"$DOTFILES_DIR/../liquidprompt/liquidprompt" ; do

		if [ -f "$src" ] ; then
			. "$src"
			return $?
		fi
	done
	return 1
}
___prompt_load_liquid && unset ___prompt_load_liquid
