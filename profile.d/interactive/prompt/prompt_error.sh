
# add error code
___prompt_command_err() {
	if [ "$1" -eq '0' ] ; then
		___prompt_err=''
	else
		___prompt_err="($1)"
	fi
	return 0
}
PROMPT_COMMAND="$PROMPT_COMMAND
___prompt_command_err \$?
"

if [ -n "$COLORTERM" ] ; then
	PS1_PREFIX="${PS1_PREFIX}\[${FG_RED_BR}\]\${___prompt_err}\[${RST}\]"
else
	PS1_PREFIX="${PS1_PREFIX}\${___prompt_err}"
fi

# set the prompt in case PROMPT_COMMAND isnt used
PS1="$PS1_PREFIX$PS1_SUFFIX"
