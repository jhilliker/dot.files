
___prompt_load_starship() {
	local cmd=
	cmd="$(starship init bash)" \
	&& [ -n "$cmd" ] \
	&& eval "$cmd"
}
isdefined starship \
&& ___prompt_load_starship \
&& unset ___prompt_load_starship
