
if [ -n "$COLORTERM" ] ; then
	: ${LS_COLORS_DIR:=${STYLE_BOLD}${FG_BLUE}} # overridden in environment.sh
	PS1_SUFFIX='\[${LS_COLORS_DIR}\]\w\['"${RST}"'\] \$ '
fi

# window titles
case "$TERM" in
	xterm*|rxvt*) PS1_PREFIX='\[\e]0;\w\a\]'"${PS1_PREFIX}" ;;
esac

if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_CONNECTION" ] || [ -n "$SSH_TTY" ] ; then
	PS1_PREFIX="${PS1_PREFIX}"'\u@\h:'
fi

PS1="$PS1_PREFIX$PS1_SUFFIX"
