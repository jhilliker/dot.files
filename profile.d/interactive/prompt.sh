
# failsafe
unset PROMPT_COMMAND
PS1_PREFIX=''
PS1_SUFFIX='\w \$ '
PS1="$PS1_PREFIX$PS1_SUFFIX"

# load best prompt available
___prompt_load() {
	local ___promptSrcDir="$DOTFILES_DIR/profile.d/interactive/prompt"

	false \
	|| . "$___promptSrcDir/prompt_starship.sh" \
	|| . "$___promptSrcDir/prompt_liquid.sh" \
	|| {
		. "$___promptSrcDir/prompt_local.sh" \
		&& . "$___promptSrcDir/prompt_error.sh" \
		&& . "$___promptSrcDir/prompt_git.sh"
	}
}
___prompt_load \
&& unset ___prompt_load ___prompt_local
