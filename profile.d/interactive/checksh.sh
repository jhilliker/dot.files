#!/bin/sh

___checksh_one() {
	local real="$(realpath -e "$1")"

	endswith '/dash' "$real" || {
		printf -- "'%b%s%b' -> '%b%s%b%s%b'\n" \
			"$FG_RED" "$1" "$RST" \
			"$FG_RED" "${real%/*}/" "$FG_RED_BR" "${real##*/}" "$RST"
		return 1
	}
}

___checksh() {
	local status=0

	# check /bin/sh
	___checksh_one '/bin/sh' \
	|| status=1

	# check `sh` if it's not /bin/sh
	[ '/bin/sh' = "$(which sh)" ] \
	|| ___checksh_one "$(which sh)" \
	|| status=1

	[ $status = 0 ] || echo 'use "dashbinsh" (AUR) to fix'

	return $status
}
___checksh && unset ___checksh ___checksh_one
