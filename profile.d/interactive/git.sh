
isdefined git || return 0

##### git settings

___git_set_setting() {
	local k="${1:?Needs key}"
	local newV="${2:?Needs new value}"
	local oldV="$(git config --global --get "$k")"

	[ "$oldV" = "$newV" ] || {
		local cmd="git config --global '$k' '$newV'"
		log INFO "Setting: $cmd"
		eval $cmd  #eval to tokenize single quotes
	}
}
___git_user_settings() {
	___git_set_setting user.name "Jeremy Hilliker"
	___git_set_setting user.email "dev.nospam@jeremyh.com"
}

##### git completion

___git_completion_isSet() {
	complete -p | grep -q 'git'
}
___git_completion() {
	# https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
	local local possibleLocations='
		/usr/share/bash-completion/completions/git  '`#arch`'
		/usr/share/git-core/contrib/completion/git-completion.bash
		/etc/bash_completion.d/git
	'
	local f
	for f in $possibleLocations ; do
		[ -f "$f" ] || continue
		! ___git_completion_isSet || return 0
		. "$f" \
			&& return 0 \
			|| log ERR "sourcing: '$f'"
	done
	return 1
}

___git_user_settings \
	&& unset ___git_set_setting ___git_user_settings \
	&& ___git_completion \
	&& unset ___git_completion ___git_completion_isSet
