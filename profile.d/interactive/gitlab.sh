
isdefined git || return 0

: ${GITLAB_EMAIL:='3305991-jhilliker@users.noreply.gitlab.com'}
#: ${GITLAB_TOKEN:=''}

: ${GITHUB_EMAIL:='53842233+jhilliker@users.noreply.github.com'}
# curl -s https://api.github.com/users/jhilliker/repos | sed -En '/^\s*"ssh_url": "/ { s/^\s*"ssh_url": "(git@git...\.com:jhilliker\/.*\.git)",?$/\1/ ; p}'

___gitlab_api() {
	local url="https://gitlab.com/api/v4/${1:?No endpoint specified}"
	local access_tokenPath="$HOME/.ssh/access_token.gitlab"

	if [ -r "$access_tokenPath" ] ; then
		local access_token="$(cat "$access_tokenPath")"
		if [ -n "$access_token" ] ; then
			if contains '\?' "$url" ; then
				url="$(printf -- '%s' "$url" | sed "s,?,?access_token=$access_token\&,")"
			else
				url="$url?access_token=$access_token"
			fi
		else
			log ERROR "'$access_tokenPath' file is empty"
		fi
	else
		log WARN "'$access_tokenPath' missing"
	fi

	log DEBUG "requesting: $(
		printf -- '%s' "$url" | sed 's/?access_token=[[:alnum:]_-]*/?access_token=*****/')"
	curl -sL "$url"
}

___gitlab_getNamespace() {
	namespace=${GITLAB_EMAIL:?"GITLAB_EMAIL unset"}
	namespace="${namespace#*-}"
	namespace="${namespace%@*}"
}

gitlabclone () {
	local repo=${1:?"Usage: gitclone remoteRepoName [localTargetDir]"}
	local namespace=
	___gitlab_getNamespace || return 1
	local url="git@gitlab.com:${namespace}/${repo}.git"
	shift
	git clone "$url" "$@"
}

___gitlab_getRepos() {
	local namespace=
	___gitlab_getNamespace || return 1
	local url="users/${namespace}/projects"
	___gitlab_api "$url" \
		| tr ',{}' '\n' \
		| sed -nE '/^\s*"ssh_url_to_repo":".*"$/ {
			s/^\s*"ssh_url_to_repo":\s?"(.*)"$/\1/
			p
		}'
}

gitlabcloneall() {
	{
		___gitlab_getRepos \
		| xargs -tr -P1 -n1 -d'\n' git clone
	} \
	&& gitst
}
