
___bash_completion() {
	local possibleLocations='
		/usr/share/bash-completion/bash_completion  '`#arch`'
		/etc/bash_completion
	'
	local f
	for f in $possibleLocations ; do
		[ -f "$f" ] || continue
		. "$f" \
			&& return 0 \
			|| log ERR "sourcing: '$f'"
	done
	log ERR "couldn't source"
	return 1

}
___bash_completion \
	&& unset ___bash_completion
