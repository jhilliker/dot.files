#!/bin/bash

function ___path_do() {
	local ___path_bins="$(realpath "${DOTFILES_DIR}/bin")"
	[ -d "$___path_bins" ] || return 1 # sanity check

	local d
	for d in "$___path_bins"/* ; do
		if [ -d "$d" ] ; then
			PATH="$d:$PATH"
		fi
	done

	PATH="$___path_bins:$PATH"
}
___path_do && unset ___path_do
