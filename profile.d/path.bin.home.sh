
# ensure ~/bin exists
[ -d "$HOME/bin" ] \
	|| mkdir -p "$HOME/bin" \
	|| { log ERR "creating ~/bin" ; return 1 ; }

# duplicates and missing directories will be pruned by path.prune.sh
PATH=\
":$HOME/bin:$HOME/.local/bin"\
":$PATH"\
':/usr/local/sbin:/usr/local/bin'\
':/usr/sbin:/usr/bin'\
':/sbin:/bin'
