#!/bin/sh -e

#
# d/l and install latest version from git
#

readonly target="$HOME/dot.files"
readonly sshRepo='git@gitlab.com:jhilliker/dot.files.git'
readonly httpRepo='https://gitlab.com/jhilliker/dot.files.git'
readonly myName='Jeremy Hilliker'
readonly myEmail='*jhilliker@users.noreply.*.com*'
readonly deleteLineRE='[Hh]illiker'
readonly sshKeygen='./profile.d/ssh.keys.sh'
readonly gitlabSSHkeyURL='https://gitlab.com/profile/keys'
readonly gitconfig="$HOME/.gitconfig"

exec >&2

type log > /dev/null 2>&1 || log() {
	printf '***** %s\n' "$*"
} >&2
type fatal > /dev/null 2>&1 || fatal() {
	log FATAL "$*"
	exit 1
}

[ "${PWD#$target}" != "$PWD" ] && fatal "Already in '$target'"

[ -e "$target" ] && {
	printf '\n'
	ls -ld "$target"
	stat "$target"
	printf '\n'
	log "'$target' exists"

	if [ "$(readlink -f .)" -ef "$(readlink -f "$target")" ] ; then
		fatal "'.' and '$target' are the same"
	fi

	log 'Overwritting'
	if [ -L "$target" ] && [ -d "$target" ] ; then
		# keep the symlink
		rm -rf "$target"/* "$target"/.[^.]* "$target"/..?*
	else
		rm -rf "$target"
	fi
}

mkdir -p "$target"

if [ -e "$gitconfig" ] || [ -L "$gitconfig" ] ; then
	rm -f "$gitconfig~"
	mv "$gitconfig" "$gitconfig~"
fi
sed "/$deleteLineRE/ d" ./home/gitconfig > "$gitconfig"

if ! ssh -T "${sshRepo%%:*}" ; then
	echo
	__SSH_KEY_GEN_FORCE='t'
	. "$sshKeygen"
	unset __SSH_KEY_GEN_FORCE
	echo
	{ which start && start "$gitlabSSHkeyURL" ; } > /dev/null 2>&1 || true
	fatal "Add above to:  $gitlabSSHkeyURL"
fi

cd
git clone "$sshRepo"
cd "$target"

if [ -e "$gitconfig~" ] || [ -L "$gitconfig~" ] ; then
	rm -f "$gitconfig"
	mv "$gitconfig~" "$gitconfig"
fi

exec ./deploy.bash
